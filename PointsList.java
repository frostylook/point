package point;

public class PointsList {
    String point;
    Double x;
    Double y;

    public PointsList(String point, Double x, Double y) {
        this.point = point;
        this.x = x;
        this.y = y;
    }

    public double getDistance() {
            double temp = Math.sqrt(x) + Math.sqrt(y);
            double result = Math.pow(temp, 2);
        return result;
    }

    @Override
    public String toString() {
        return "PointsList{" +
                "point='" + point + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}