package point;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Point {
    static Scanner scanner = new Scanner(System.in);
    static List<PointsList> pointsList = new ArrayList<>();

    public static void main(String[] args) {

        System.out.println("Write amount of points:");
        int amount = scanner.nextInt();

        for (int i = 0; i < amount; i++) {
            System.out.println("Write a name of point:");
            String point = scanner.next();
            System.out.println("Write x:");
            double x = scanner.nextDouble();
            System.out.println("Write y:");
            double y = scanner.nextDouble();

            PointsList pointsList1 = new PointsList(point, x, y);
            pointsList.add(pointsList1);
        }

        printList(pointsList);
        sortPoints();
        printArray();
    }

    private static void printList(List<PointsList> pointsList) {
        for (PointsList x : pointsList)
            System.out.println(pointsList.indexOf(x) + " " + x);
    }

    private static void sortPoints() {
        pointsList.sort((o1, o2) -> (int) (o1.getDistance() - o2.getDistance()));
    }

    private static void printArray() {
        for (int i = 0; i < pointsList.size(); i++) {
            System.out.println(pointsList.get(i) + " Distance: " + pointsList.get(i).getDistance());
        }
    }
}